<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file:
 *
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example:
 */

$show_overlay_first = 0;
$show_overlay_second = 0;
$show_overlay_third = 0;

if ($settings->title__project_first || $settings->subtitle__project_first || $settings->body__project_first || ($settings->link_url__project_first && $settings->link_text__project_first)) :
  $show_overlay_first = 1;
endif;

if ($settings->title__project_second || $settings->subtitle__project_second || $settings->body__project_second || ($settings->link_url__project_second && $settings->link_text__project_second)) :
  $show_overlay_second = 1;
endif;

if ($settings->title__project_third || $settings->subtitle__project_third || $settings->body__project_third || ($settings->link_url__project_third && $settings->link_text__project_third)) :
  $show_overlay_third = 1;
endif;

?>

<div class="bb-module bb-module--featured-projects">

  <h1 class="bb-featured-projects--title"><?php echo $settings->block_title; ?></h1>

  <div class="bb-featured-projects">

    <div class="bb-featured-project bb-featured-project--first">
      <div class="bb-featured-project-image">
        <?php if ($settings->image__project_first) : ?>
          <div class="inner <?php if ($show_overlay_first) : ?>image-with-overlay<?php endif; ?>">
            <?php echo wp_get_attachment_image( $settings->image__project_first, $size = 'walsh-featured-project'); ?>
            <?php if ($show_overlay_first) : ?>
              <div class="bg-overlay"></div>
              <div class="bg-underlay"></div>
            <?php endif; ?>
          </div>
        <?php endif; ?>
      </div>

      <div class="bb-featured-project-text">
        <?php if ($settings->title__project_first) : ?>
          <h3 class="bb-featured-projects--title"><?php echo $settings->title__project_first; ?></h3>
        <?php endif; ?>
        <?php if ($settings->subtitle__project_first) : ?>
          <h4 class="bb-featured-projects--subtitle"><?php echo $settings->subtitle__project_first; ?></h4>
        <?php endif; ?>
        <?php if ($settings->body__project_first) : ?>
          <p class="bb-featured-projects--body"><?php echo $settings->body__project_first; ?></p>
        <?php endif; ?>
        <?php if ($settings->link_url__project_first && $settings->link_text__project_first) : ?>
          <div class="cta cta--bb-featured-project">
            <a class="cta-link" href="<?php echo $settings->link_url__project_first; ?>"><?php echo $settings->link_text__project_first; ?></a>
          </div>
        <?php endif; ?>
      </div>
    </div>

    <div class="bb-featured-project bb-featured-project--second">
      <div class="bb-featured-project-image">
        <?php if ($settings->image__project_second) : ?>
          <div class="inner <?php if ($show_overlay_second) : ?>image-with-overlay<?php endif; ?>">
            <?php echo wp_get_attachment_image( $settings->image__project_second, $size = 'walsh-featured-project'); ?>
            <?php if ($show_overlay_second) : ?>
              <div class="bg-overlay"></div>
              <div class="bg-underlay"></div>
            <?php endif; ?>
          </div>
        <?php endif; ?>
      </div>

      <div class="bb-featured-project-text">
        <?php if ($settings->title__project_second) : ?>
          <h3 class="bb-featured-projects--title"><?php echo $settings->title__project_second; ?></h3>
        <?php endif; ?>
        <?php if ($settings->subtitle__project_second) : ?>
          <h4 class="bb-featured-projects--subtitle"><?php echo $settings->subtitle__project_second; ?></h4>
        <?php endif; ?>
        <?php if ($settings->body__project_second) : ?>
          <p class="bb-featured-projects--body"><?php echo $settings->body__project_second; ?></p>
        <?php endif; ?>
        <?php if ($settings->link_url__project_second && $settings->link_text__project_second) : ?>
          <div class="cta bb-two-columns-no-photos--link">
            <a class="cta-link" href="<?php echo $settings->link_url__project_second; ?>"><?php echo $settings->link_text__project_second; ?></a>
          </div>
        <?php endif; ?>
      </div>
    </div>

    <div class="bb-featured-project bb-featured-project--third">
      <div class="bb-featured-project-image">
        <?php if ($settings->image__project_third) : ?>
          <div class="inner <?php if ($show_overlay_third) : ?>image-with-overlay<?php endif; ?>">
            <?php echo wp_get_attachment_image( $settings->image__project_third, $size = 'walsh-featured-project'); ?>
            <?php if ($show_overlay_third) : ?>
              <div class="bg-overlay"></div>
              <div class="bg-underlay"></div>
            <?php endif; ?>
          </div>
        <?php endif; ?>
      </div>

      <div class="bb-featured-project-text">
        <?php if ($settings->title__project_third) : ?>
          <h3 class="bb-featured-projects--title"><?php echo $settings->title__project_third; ?></h3>
        <?php endif; ?>
        <?php if ($settings->subtitle__project_third) : ?>
          <h4 class="bb-featured-projects--subtitle"><?php echo $settings->subtitle__project_third; ?></h4>
        <?php endif; ?>
        <?php if ($settings->body__project_third) : ?>
          <p class="bb-featured-projects--body"><?php echo $settings->body__project_third; ?></p>
        <?php endif; ?>
        <?php if ($settings->link_url__project_third && $settings->link_text__project_third) : ?>
          <div class="cta bb-two-columns-no-photos--link">
            <a class="cta-link" href="<?php echo $settings->link_url__project_third; ?>"><?php echo $settings->link_text__project_third; ?></a>
          </div>
        <?php endif; ?>
      </div>
    </div>

  </div>

</div>
