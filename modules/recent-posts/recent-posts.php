<?php

/**
 * Custom Beaver Builder module: Recent Posts.
 *
 * @class RecentPostsModule
 */
class RecentPostsModule extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Recent Posts', 'fl-builder'),
            'description'   => __('Displays four most recent posts.', 'fl-builder'),
            'category'		=> __('Custom Modules', 'fl-builder'),
            'dir'           => CUSTOM_BB_MODULES_DIR . 'modules/recent-posts/',
            'url'           => CUSTOM_BB_MODULES_URL . 'modules/recent-posts/',
            'icon'          => 'format-image.svg',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('RecentPostsModule', array(
  'content-tab'          => array(
    'title'              => __( 'Content', 'fl-builder' ),
    'sections'           => array(
      'section--content' => array(
        'title'          => __( 'Content', 'fl-builder' ),
        'fields'         => array(
          'title'        => array(
            'type'       => 'text',
            'label'      => __( 'Title', 'fl-builder' ),
          ),
          'link_text'    => array(
            'type'       => 'text',
            'label'      => __( 'Link Text (under body text)', 'fl-builder' ),
          ),
          'link_url'     => array(
            'type'       => 'link',
            'label'      => __( 'URL', 'fl-builder' ),
          )
        )
      )
    )
  )
));
