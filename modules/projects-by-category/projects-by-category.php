<?php

/**
 * Custom Beaver Builder module: Projects By Category.
 *
 * @class ProjectsByCategoryModule
 */
class ProjectsByCategoryModule extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Projects By Category', 'fl-builder'),
            'description'   => __('Layout to be used for projects by category module', 'fl-builder'),
            'category'		=> __('Custom Modules', 'fl-builder'),
            'dir'           => CUSTOM_BB_MODULES_DIR . 'modules/projects-by-category/',
            'url'           => CUSTOM_BB_MODULES_URL . 'modules/projects-by-category/',
            'icon'          => 'format-image.svg',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('ProjectsByCategoryModule', array(

  'image'      => array(
    'title'              => __( 'Image', 'fl-builder' ),
    'sections'           => array(
      'section--image'  => array(
        'title'  => '',
        'fields'        => array(
          'image' => array(
            'type'          => 'photo',
            'label'         => __('Image', 'fl-builder'),
            'show_remove'   => false,
          ),
          'icon' => array(
            'type'          => 'photo',
            'label'         => __('Icon', 'fl-builder'),
            'show_remove'   => false,
          )
        )
      ),
    )
  ),

  'text'       => array(
    'title'    => __( 'Text', 'fl-builder' ),
    'sections' => array(
      'section--body-text' => array(
        'title'  => '',
        'fields'         => array(
          'title'     => array(
            'type'       => 'text',
            'label'      => __( 'Title', 'fl-builder' ),
          ),
          'subtitle'     => array(
            'type'       => 'text',
            'label'      => __( 'Subtitle', 'fl-builder' ),
          ),
          'body' => array(
            'type'          => 'editor',
            'media_buttons' => false,
            'wpautop'       => true,
            'label'      => __( 'Body Text', 'fl-builder' ),
            'default'    => ''
          )
        )
      )
    )
  ),

  'projects' => array(
    'title'    => __( 'Projects', 'fl-builder' ),
    'sections' => array(
      'general' => array(
        'title'  => '',
        'fields' => array(
          'items' => array(
            'type'         => 'form',
            'label'        => __( 'Project', 'fl-builder' ),
            'form'         => 'projects_form', // ID from registered form below
            'preview_text' => 'label', // Name of a field to use for the preview text
            'multiple'     => true,
          ),
        ),
      )
    )
  )
));

/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('projects_form', array(
  'title' => __( 'Add Item', 'fl-builder' ),
  'tabs'  => array(
    'general' => array(
      'title'    => __( 'General', 'fl-builder' ),
      'sections' => array(
        'general' => array(
          'title'  => '',
          'fields' => array(
            'label' => array(
              'type'        => 'text',
              'label'       => __( 'Label', 'fl-builder' ),
              'connections' => array( 'string' ),
            ),
          ),
        ),
        'link' => array(
          'title'  => '',
          'fields' => array(
            'link' => array(
              'type'          => 'link',
              'label'         => __('Link Field', 'fl-builder')
            ),
          ),
        ),
      ),
    ),
  ),
));
