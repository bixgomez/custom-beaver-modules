jQuery(function($){

  $(document).ready(function(){

    $(".no-fouc").removeClass("no-fouc");

    $( ".fl-col-group" ).
      addClass( "standard-col-group" );

    $( ".bb-module" ).closest( ".fl-col-group" ).
      removeClass( "standard-col-group" ).
      addClass( "custom-col-group" );

    $( ".fl-module" ).
      addClass( "standard-module" );

    $( ".bb-module" ).closest( ".fl-module" ).
      removeClass( "standard-module" ).
      addClass( "custom-module" );

    $(".fl-col-group").each(function() {
      var numItems = $( this ).find( "> .fl-col" ).length;
      if ( numItems > 1 ) {
        $( this ).addClass("multiple");
      }
    });

  });

});
