<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file:
 *
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example:
 */

?>

<div class="bb-module bb-module--four-column-icons ">
  <div class="module-header module-header--four-column-icons">
    <h2 class="title title--four-column-icons"><?php echo $settings->title; ?></h2>
    <h3 class="subtitle subtitle--four-column-icons"><?php echo $settings->subtitle; ?></h3>
    <div class="body body--four-column-icons"><?php echo $settings->body; ?></div>
  </div>
  <div class="cols cols--four-column-icons">
    <?php for ( $i = 0; $i < count( $settings->item ); $i++ ) : ?>
      <?php
      if ( empty( $settings->item[ $i ] ) ) {
        continue;
      }
      $icon_id = $settings->item[ $i ]->item_icon;
      $icon_image = wp_get_attachment_image( $icon_id, $size = 'thumbnail');
      $subtitle = $settings->item[ $i ]->item_title;
      $body_text = $settings->item[ $i ]->item_body;
      ?>
      <div class="col">
        <div class="col-icon">
          <?php print $icon_image; ?>
        </div>
        <h3 class="subtitle">
          <?php print $subtitle ?>
        </h3>
        <div class="bodytext">
          <?php print $body_text ?>
        </div>
        <ul class="links">
          <?php for ( $x = 0; $x < count( $settings->item[ $i ]->item_links ); $x++ ) : ?>
            <?php $linksArray = json_decode( $settings->item[ $i ]->item_links[ $x ], true ); ?>
            <li><a href="<?php echo $linksArray['item_link_url']; ?>"><?php echo $linksArray['item_link_text']; ?></a></li>
          <?php endfor; ?>
        </ul>
      </div>
    <?php endfor; ?>
  </div>
</div>
