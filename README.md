# Walsh Construction Custom Beaver Builder Modules

This is a custom WordPress plugin  featuring several custom Beaver Builder modules, including Sass theming files. I wrote in support of some development work 
I recently completed for Walsh, a local construction firm.

The staging site (it has not yet launched) can be found here:
[http://staging.sincere-drum.flywheelsites.com/](http://staging.sincere-drum.flywheelsites.com/)

The site was built using the popular Beaver Builder page builder plugin, which allows the end user
full control over their site's pages.  It features a self-contained, proprietary WYSIWYG editor, and
a set of base "modules" (layout elements, similar to "blocks" in the core WordPress content editor, or
"paragraphs" or "bricks" in Drupal) which can be placed anywhere on a Beaver Builder-enabled page.

This plugin introduces 10 custom modules that were design to support design work done specifically for
the end client.  Many of these, however, can be used on any site.

## The Modules

* Featured Projects
* Four Columns with Hover Effect
* Four Columns with Icons
* One Column with Text
* Photo with Text
* Projects By Category
* Recent Posts
* Text with Heading
* Two Columns with Photos
* Video Banner
