<?php
	
/**
 * A class that handles loading custom modules and custom
 * fields if the builder is installed and activated.
 */
class FL_Custom_Modules_Example_Loader {
	
	/**
	 * Initializes the class once all plugins have loaded.
	 */
	static public function init() {
		add_action( 'plugins_loaded', __CLASS__ . '::setup_hooks' );
	}
	
	/**
	 * Setup hooks if the builder is installed and activated.
	 */
	static public function setup_hooks() {
		if ( ! class_exists( 'FLBuilder' ) ) {
			return;	
		}
		
		// Load custom modules.
		add_action( 'init', __CLASS__ . '::load_modules' );
		
		// Register custom fields.
		add_filter( 'fl_builder_custom_fields', __CLASS__ . '::register_fields' );
		
		// Enqueue custom field assets.
		add_action( 'init', __CLASS__ . '::enqueue_field_assets' );
	}
	
	/**
	 * Loads our custom modules.
	 */
	static public function load_modules() {
    require_once CUSTOM_BB_MODULES_DIR . 'modules/one-column-text/one-column-text.php';
    require_once CUSTOM_BB_MODULES_DIR . 'modules/two-columns-with-photos/two-columns-with-photos.php';
    require_once CUSTOM_BB_MODULES_DIR . 'modules/featured-projects/featured-projects.php';
    require_once CUSTOM_BB_MODULES_DIR . 'modules/photo-with-text/photo-with-text.php';
    require_once CUSTOM_BB_MODULES_DIR . 'modules/recent-posts/recent-posts.php';
    require_once CUSTOM_BB_MODULES_DIR . 'modules/four-column-icons/four-column-icons.php';
    require_once CUSTOM_BB_MODULES_DIR . 'modules/four-column-hover/four-column-hover.php';
    require_once CUSTOM_BB_MODULES_DIR . 'modules/projects-by-category/projects-by-category.php';
    require_once CUSTOM_BB_MODULES_DIR . 'modules/video-banner/video-banner.php';
    }
	
	/**
	 * Registers our custom fields.
	 */
	static public function register_fields( $fields ) {
		$fields['my-custom-field'] = CUSTOM_BB_MODULES_DIR . 'fields/my-custom-field.php';
		return $fields;
	}
	
	/**
	 * Enqueues our custom field assets only if the builder UI is active.
	 */
	static public function enqueue_field_assets() {
		if ( ! FLBuilderModel::is_builder_active() ) {
			return;
		}
		
		wp_enqueue_style( 'my-custom-fields', CUSTOM_BB_MODULES_URL . 'assets/css/fields.css', array(), '' );
		wp_enqueue_script( 'my-custom-fields', CUSTOM_BB_MODULES_URL . 'assets/js/fields.js', array(), '', true );
	}
}

FL_Custom_Modules_Example_Loader::init();
