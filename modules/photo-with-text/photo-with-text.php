<?php

/**
 * Custom Beaver Builder module: Photo with Text.
 *
 * @class PhotoWithTextModule
 */
class PhotoWithTextModule extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Single Photo With Text', 'fl-builder'),
            'description'   => __('Layout to be used for photo with or without text', 'fl-builder'),
            'category'		=> __('Custom Modules', 'fl-builder'),
            'dir'           => CUSTOM_BB_MODULES_DIR . 'modules/photo-with-text/',
            'url'           => CUSTOM_BB_MODULES_URL . 'modules/photo-with-text/',
            'icon'          => 'format-image.svg',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('PhotoWithTextModule', array(

  'content-tab'          => array(
    'title'              => __( 'Content', 'fl-builder' ),

    'sections'           => array(

      'section--photo'  => array(
        'title'            => __( 'Photo', 'fl-builder' ),
        'fields'        => array(
          'image_text_config'   => array(
            'type'       => 'button-group',
            'label'      => 'Image/Text Config',
            'default'    => 'image_behind',
            'options'    => array(
              'image_only' => 'Photo only',
              'image_behind' => 'Photo background',
              'image_left'   => 'Photo left',
              'image_right'  => 'Photo right'
            ),
          ),
          'image' => array(
            'type'          => 'photo',
            'label'         => __('Photo Field', 'fl-builder'),
            'show_remove'   => false,
          )
        )
      ),

      'section--body-text' => array(
        'title'          => __( 'Body Text', 'fl-builder' ),
        'fields'         => array(
          'body' => array(
            'type'          => 'editor',
            'media_buttons' => false,
            'wpautop'       => true,
            'label'      => __( 'Body Text', 'fl-builder' ),
            'default'    => ''
          )
        )
      ),

      'section--other-text' => array(
        'title'          => __( 'Other Text', 'fl-builder' ),
        'fields'         => array(
          'title'     => array(
            'type'       => 'text',
            'label'      => __( 'Title', 'fl-builder' ),
          ),
          'subtitle'     => array(
            'type'       => 'text',
            'label'      => __( 'Subtitle', 'fl-builder' ),
          ),
          'link_text'    => array(
            'type'       => 'text',
            'label'      => __( 'Link Text (under body text)', 'fl-builder' ),
          ),
          'link_url'     => array(
            'type'       => 'link',
            'label'      => __( 'URL', 'fl-builder' ),
          )
        )
      )
    )
  )
));
