<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file:
 *
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example:
 */

?>

<div class="bb-module bb-module--two-columns-with-photos <?php echo $layout_class; ?>">

  <?php if ($settings->title || $settings->subtitle) : ?>
    <div class="module-header module-header--two-columns-with-photos">
      <?php if ($settings->title) : ?>
        <h1 class="title title--two-columns-with-photos"><?php echo $settings->title; ?></h1>
      <?php endif; ?>
      <?php if ($settings->subtitle) : ?>
        <h2 class="subtitle subtitle--two-columns-with-photos"><?php echo $settings->subtitle; ?></h2>
      <?php endif; ?>
    </div>
  <?php endif; ?>

  <div class="cols cols--two-columns-with-photos">
    <?php for($n=1; $n<=2; $n++): ?>
      <?php
      $this_image = $settings->{'image__col_' . $n};
      $this_body = $settings->{'body__col_' . $n};
      ?>
      <div class="col col-<?php echo $n ?>">
        <?php if ($this_image) : ?>
          <div class="image">
            <?php
            // echo wp_get_attachment_image_src($this_image)[0];
            echo wp_get_attachment_image($this_image, $size = "walsh-wide-feature");
            ?>
          </div>
        <?php endif; ?>
        <?php if ($this_body) : ?>
          <div class="text"><?php echo $this_body; ?></div>
        <?php endif; ?>
      </div>
    <?php endfor; ?>
  </div>

</div>
