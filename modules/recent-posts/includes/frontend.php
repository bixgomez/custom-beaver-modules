<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file:
 *
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example:
 */

?>

<div class="bb-module bb-module--recent-posts">
  <div class="bb--recent-posts--text">

    <?php if ($settings->title) : ?>
      <h1 class="bb--recent-posts--title"><?php echo $settings->title; ?></h1>
    <?php endif; ?>

    <div class="news-items">
      <?php $the_query = new WP_Query( 'posts_per_page=4' ); ?>
      <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
        <div class="news-item">
          <div class="news-item--image">
            <p class="news-item--thumbnail"><?php the_post_thumbnail( 'walsh-news-thumbnail' ); ?></p>
          </div>
          <div class="news-item--text">
            <?php the_date('F jS Y', '<h6 class="news-item--date">', '</h6>'); ?>
            <h3 class="news-item--title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
            <p class="news-item--excerpt"><?php echo get_the_excerpt(); ?></p>
            <a class="news-item--more" href="<?php the_permalink() ?>">Read More »</a>
          </div>
        </div>
      <?php endwhile; wp_reset_postdata(); ?>
    </div>

    <?php if ($settings->link_url && $settings->link_text) : ?>
      <div class="bb--recent-posts--link">
        <a href="<?php echo $settings->link_url; ?>"><?php echo $settings->link_text; ?></a>
      </div>
    <?php endif; ?>

  </div>
</div>
