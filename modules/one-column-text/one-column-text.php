<?php

/**
 * Custom Beaver Builder module: One Column with Text.
 *
 * @class OneColumnTextModule
 */
class OneColumnTextModule extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('One Column Text', 'fl-builder'),
            'description'   => __('Layout to be used for one column text.', 'fl-builder'),
            'category'		=> __('Custom Modules', 'fl-builder'),
            'dir'           => CUSTOM_BB_MODULES_DIR . 'modules/one-column-text/',
            'url'           => CUSTOM_BB_MODULES_URL . 'modules/one-column-text/',
            'icon'          => 'format-image.svg',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('OneColumnTextModule', array(
  'content-tab'          => array(
    'title'              => __( 'Content', 'fl-builder' ),
    'sections'           => array(
      'section--content' => array(
        'title'          => __( 'Content', 'fl-builder' ),
        'fields'         => array(
          'title'        => array(
            'type'       => 'text',
            'label'      => __( 'Title', 'fl-builder' ),
          ),
          'subtitle'     => array(
            'type'       => 'text',
            'label'      => __( 'Subtitle', 'fl-builder' ),
          ),
          'heading'     => array(
            'type'       => 'text',
            'label'      => __( 'Heading', 'fl-builder' ),
          ),
          'body' => array(
            'type'          => 'editor',
            'media_buttons' => false,
            'wpautop'       => true,
            'label'      => __( 'Body Text', 'fl-builder' ),
            'default'    => ''
          ),
          'link_text'    => array(
            'type'       => 'text',
            'label'      => __( 'Link Text (under body text)', 'fl-builder' ),
          ),
          'link_url'     => array(
            'type'       => 'link',
            'label'      => __( 'URL', 'fl-builder' ),
          )
        )
      )
    )
  )
));
