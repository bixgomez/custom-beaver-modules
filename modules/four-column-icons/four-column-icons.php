<?php

/**
 * Custom Beaver Builder module: Four Columns with Icons.
 *
 * @class FourColumnIconsModule
 */
class FourColumnIconsModule extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Four Column Text w/ Icons', 'fl-builder'),
            'description'   => __('Layout to be used for four columns of text with icons', 'fl-builder'),
            'category'		=> __('Custom Modules', 'fl-builder'),
            'dir'           => CUSTOM_BB_MODULES_DIR . 'modules/four-column-icons/',
            'url'           => CUSTOM_BB_MODULES_URL . 'modules/four-column-icons/',
            'icon'          => 'format-image.svg',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('FourColumnIconsModule', array(

  'text'       => array(
    'title'    => __( 'Intro Text', 'fl-builder' ),
    'sections' => array(
      'section--body-text' => array(
        'title'  => '',
        'fields'         => array(
          'title'     => array(
            'type'       => 'text',
            'label'      => __( 'Intro Title', 'fl-builder' ),
          ),
          'subtitle'     => array(
            'type'       => 'text',
            'label'      => __( 'Intro Subtitle', 'fl-builder' ),
          ),
          'body' => array(
            'type'          => 'editor',
            'media_buttons' => false,
            'wpautop'       => true,
            'label'      => __( 'Intro Body Text', 'fl-builder' ),
            'default'    => ''
          )
        )
      )
    )
  ),

  'items' => array(
    'title'    => __( 'Items', 'fl-builder' ),
    'sections' => array(
      'general' => array(
        'title'  => '',
        'fields' => array(
          'item' => array(
            'type'         => 'form',
            'label'        => __( 'Item', 'fl-builder' ),
            'form'         => 'items_form', // ID from registered form below
            'preview_text' => 'label', // Name of a field to use for the preview text
            'multiple'     => true,
          ),
        ),
      )
    )
  )
));

/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('items_form', array(
  'title' => __( 'Add Item', 'fl-builder' ),
  'tabs'  => array(
    'general' => array(
      'title'    => __( 'General', 'fl-builder' ),
      'sections' => array(
        'general' => array(
          'title'  => '',
          'fields' => array(
            'item_icon' => array(
              'type'          => 'photo',
              'label'         => __('Item Icon', 'fl-builder'),
              'show_remove'   => true,
            ),
            'item_title'     => array(
              'type'       => 'text',
              'label'      => __( 'Item Title', 'fl-builder' ),
            ),
            'item_links' => array(
              'type'         => 'form',
              'label'        => __( 'Item Link', 'fl-builder' ),
              'form'         => 'items_links_form', // ID from registered form below
              'preview_text' => 'label', // Name of a field to use for the preview text
              'multiple'     => true,
            ),
            'item_body' => array(
              'type'          => 'editor',
              'media_buttons' => false,
              'wpautop'       => true,
              'label'      => __( 'Item Body Text', 'fl-builder' ),
              'default'    => ''
            ),
          ),
        ),
      ),
    ),
  ),
));

/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('items_links_form', array(
  'title' => __( 'Add Link', 'fl-builder' ),
  'tabs'  => array(
    'general' => array(
      'title'    => __( 'General', 'fl-builder' ),
      'sections' => array(
        'general' => array(
          'title'  => '',
          'fields' => array(
            'item_link_text'     => array(
              'type'       => 'text',
              'label'      => __( 'Link Text', 'fl-builder' ),
            ),
            'item_link_url' => array(
              'type'       => 'link',
              'label'      => __( 'URL', 'fl-builder' ),
            ),
          ),
        ),
      ),
    ),
  ),
));
