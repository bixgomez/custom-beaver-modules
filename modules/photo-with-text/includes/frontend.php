<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file:
 *
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example:
 */

?>

<?php

$image_only = 1;

if ($settings->title || $settings->subtitle || $settings->body) :
  $image_only = 0;
endif;

if ($settings->image_text_config) :
  $layout = $settings->image_text_config;
elseif ($image_only) :
  $layout = 'image_only';
else :
  $layout = 'image_left';
endif;

$layout_class = str_replace("_","-",$layout);

if ($layout == 'image_left' || $layout == 'image_right') :
  $image_size = 'walsh-featured-project';
else :
  $image_size = 'walsh-work-slide';
endif;

if ($layout == 'image_only' || $layout == 'image_behind') :
  $layout_class .= ' full-bleed';
endif;

?>

<div class="bb-module bb-module--photo-with-text <?php echo $layout_class; ?>">

  <?php if ( ($layout == 'image_only' || $layout == 'image_left') && ($settings->image) ) : ?>
    <div class="photo-with-text--image">
      <img src="<?php echo wp_get_attachment_image_src($settings->image, $size = $image_size)[0]; ?>">
    </div>
  <?php endif; ?>

  <?php if ( ($layout == 'image_behind') && ($settings->image) ) : ?>
    <div class="photo-with-text--image">
      <img src="<?php echo wp_get_attachment_image_src($settings->image, $size = $image_size)[0]; ?>">
      <div class="bg-overlay"></div>
      <div class="bg-underlay"></div>
    </div>
  <?php endif; ?>

  <?php if ($layout != 'image_only') : ?>
    <div class="photo-with-text--text">
      <div class="module-header module-header--photo-with-text">
        <?php if ($settings->title) : ?>
          <h1 class="title title--photo-with-text"><?php echo $settings->title; ?></h1>
        <?php endif; ?>

        <?php if ($settings->subtitle) : ?>
          <h2 class="subtitle subtitle--photo-with-text"><?php echo $settings->subtitle; ?></h2>
        <?php endif; ?>
      </div>
      <?php if ($settings->body) : ?>
        <div class="body body--photo-with-text">
          <?php echo $settings->body; ?>
        </div>
      <?php endif; ?>
      <?php if ($settings->link_url && $settings->link_text) : ?>
        <div class="cta cta--photo-with-text">
          <a class="cta-link cta-link--photo-with-text" href="<?php echo $settings->link_url; ?>"><?php echo $settings->link_text; ?></a>
        </div>
      <?php endif; ?>
    </div>
  <?php endif; ?>

  <?php if ( ($layout == 'image_right') && ($settings->image) ) : ?>
    <div class="photo-with-text--image">
      <img src="<?php echo wp_get_attachment_image_src($settings->image, $size = $image_size)[0]; ?>">
    </div>
  <?php endif; ?>

</div>
