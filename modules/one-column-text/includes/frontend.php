<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file:
 *
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example:
 */

?>

<div class="bb-module bb-module--one-column-text">
  <div class="bb--one-column-text--text">

    <?php if ($settings->title) : ?>
      <h1 class="bb--one-column-text--title"><?php echo $settings->title; ?></h1>
    <?php endif; ?>

    <?php if ($settings->subtitle) : ?>
      <h2 class="bb--one-column-text--subtitle"><?php echo $settings->subtitle; ?></h2>
    <?php endif; ?>

    <?php if ($settings->heading) : ?>
      <h3 class="bb--one-column-text--heading"><?php echo $settings->heading; ?></h3>
    <?php endif; ?>

    <?php if ($settings->body) : ?>
      <div class="bb--one-column-text--body">
        <?php echo $settings->body; ?>
      </div>
    <?php endif; ?>

    <?php if ($settings->link_url && $settings->link_text) : ?>
      <div class="bb--one-column-text--link">
        <a href="<?php echo $settings->link_url; ?>"><?php echo $settings->link_text; ?></a>
      </div>
    <?php endif; ?>

  </div>
</div>
