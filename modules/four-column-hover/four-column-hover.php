<?php

/**
 * Custom Beaver Builder module: Four Columns with Hover Effect.
 *
 * @class FourColumnHoverModule
 */
class FourColumnHoverModule extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Four Column Text w/ Hover', 'fl-builder'),
            'description'   => __('Layout to be used for four columns of images with hover text', 'fl-builder'),
            'category'		=> __('Custom Modules', 'fl-builder'),
            'dir'           => CUSTOM_BB_MODULES_DIR . 'modules/four-column-hover/',
            'url'           => CUSTOM_BB_MODULES_URL . 'modules/four-column-hover/',
            'icon'          => 'format-image.svg',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('FourColumnHoverModule', array(

  'header'          => array(
    'title'              => __( 'Header', 'fl-builder' ),

    'sections'           => array(

      'section--title' => array(
        'title'          => __( 'Title', 'fl-builder' ),
        'fields'         => array(
          'title'     => array(
            'type'       => 'text',
            'label'      => __( 'Title', 'fl-builder' ),
          )
        )
      )
    )
  ),

  'col-1'          => array(
    'title'              => __( 'Col 1', 'fl-builder' ),

    'sections'           => array(

      'section--image'  => array(
        'title'            => __( 'Icon', 'fl-builder' ),
        'fields'        => array(
          'image__col_1' => array(
            'type'          => 'photo',
            'label'         => __('Icon', 'fl-builder'),
            'show_remove'   => false,
          )
        )
      ),

      'section--body-text' => array(
        'title'          => __( 'Text', 'fl-builder' ),
        'fields'         => array(
          'title__col_1'     => array(
            'type'       => 'text',
            'label'      => __( 'Title', 'fl-builder' ),
          ),
          'body__col_1' => array(
            'type'          => 'editor',
            'media_buttons' => false,
            'wpautop'       => true,
            'label'      => __( 'Body Text', 'fl-builder' ),
            'default'    => ''
          )
        )
      )
    )
  ),

  'col-2'          => array(
  'title'              => __( 'Col 2', 'fl-builder' ),

  'sections'           => array(

    'section--image'  => array(
      'title'            => __( 'Icon', 'fl-builder' ),
      'fields'        => array(
        'image__col_2' => array(
          'type'          => 'photo',
          'label'         => __('Icon', 'fl-builder'),
          'show_remove'   => false,
        )
      )
    ),

    'section--body-text' => array(
      'title'          => __( 'Text', 'fl-builder' ),
      'fields'         => array(
        'title__col_2'     => array(
          'type'       => 'text',
          'label'      => __( 'Title', 'fl-builder' ),
        ),
        'body__col_2' => array(
          'type'          => 'editor',
          'media_buttons' => false,
          'wpautop'       => true,
          'label'      => __( 'Body Text', 'fl-builder' ),
          'default'    => ''
        )
      )
    )
  )
),

  'col-3'          => array(
  'title'              => __( 'Col 3', 'fl-builder' ),

  'sections'           => array(

    'section--image'  => array(
      'title'            => __( 'Icon', 'fl-builder' ),
      'fields'        => array(
        'image__col_3' => array(
          'type'          => 'photo',
          'label'         => __('Icon', 'fl-builder'),
          'show_remove'   => false,
        )
      )
    ),

    'section--body-text' => array(
      'title'          => __( 'Text', 'fl-builder' ),
      'fields'         => array(
        'title__col_3'     => array(
          'type'       => 'text',
          'label'      => __( 'Title', 'fl-builder' ),
        ),
        'body__col_3' => array(
          'type'          => 'editor',
          'media_buttons' => false,
          'wpautop'       => true,
          'label'      => __( 'Body Text', 'fl-builder' ),
          'default'    => ''
        )
      )
    )
  )
),

  'col-4'          => array(
  'title'              => __( 'Col 4', 'fl-builder' ),

  'sections'           => array(

    'section--image'  => array(
      'title'            => __( 'Icon', 'fl-builder' ),
      'fields'        => array(
        'image__col_4' => array(
          'type'          => 'photo',
          'label'         => __('Icon', 'fl-builder'),
          'show_remove'   => false,
        )
      )
    ),

    'section--body-text' => array(
      'title'          => __( 'Text', 'fl-builder' ),
      'fields'         => array(
        'title__col_4'     => array(
          'type'       => 'text',
          'label'      => __( 'Title', 'fl-builder' ),
        ),
        'body__col_4' => array(
          'type'          => 'editor',
          'media_buttons' => false,
          'wpautop'       => true,
          'label'      => __( 'Body Text', 'fl-builder' ),
          'default'    => ''
        )
      )
    )
  )
)
));
