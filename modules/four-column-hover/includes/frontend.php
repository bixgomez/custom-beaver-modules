<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file:
 *
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example:
 */

?>

<div class="bb-module bb-module--four-column-hover <?php echo $layout_class; ?>">
  <?php if ($settings->title) : ?>
    <div class="module-header module-header--four-column-hover">
      <h2 class="title title--four-column-hover"><?php echo $settings->title; ?></h2>
    </div>
  <?php endif; ?>
  <div class="cols cols--four-column-hover">
    <?php for($n=1; $n<=4; $n++): ?>
      <?php
      $this_image = $settings->{'image__col_' . $n};
      $this_title = $settings->{'title__col_' . $n};
      $this_body = $settings->{'body__col_' . $n};
      ?>
      <div class="col col-<?php echo $n ?>">

        <?php if ($this_image) : ?>
          <div class="image">
            <?php echo wp_get_attachment_image($this_image, $size = "walsh-hover-image"); ?>
          </div>
        <?php endif; ?>

        <?php if ($this_title || $this_body) : ?>
          <div class="text">
            <?php if ($this_title) : ?>
              <h4 class="subtitle"><?php echo $this_title; ?></h4>
            <?php endif; ?>
            <?php if ($this_body) : ?>
              <div class="bodytext"><?php echo $this_body; ?></div>
            <?php endif; ?>
          </div>
        <?php endif; ?>

      </div>
    <?php endfor; ?>
  </div>
</div>
