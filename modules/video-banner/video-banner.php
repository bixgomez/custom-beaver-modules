<?php

/**
 * Custom Beaver Builder module: Video Banner.
 *
 * @class VideoBannerModule
 */
class VideoBannerModule extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Video Banner', 'fl-builder'),
            'description'   => __('Layout to be used for photo with or without text', 'fl-builder'),
            'category'		=> __('Custom Modules', 'fl-builder'),
            'dir'           => CUSTOM_BB_MODULES_DIR . 'modules/video-banner/',
            'url'           => CUSTOM_BB_MODULES_URL . 'modules/video-banner/',
            'icon'          => 'format-image.svg',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VideoBannerModule', array(

  'content-tab'          => array(
    'title'              => __( 'Content', 'fl-builder' ),

    'sections'           => array(

      'section--video'  => array(
        'title'            => __( 'Video', 'fl-builder' ),
        'fields'        => array(
          'video' => array(
            'type'          => 'video',
            'label'         => __( 'Video', 'fl-builder' )
          ),
        )
      ),

      'section--body-text' => array(
        'title'          => __( 'Body Text', 'fl-builder' ),
        'fields'         => array(
          'body' => array(
            'type'          => 'editor',
            'media_buttons' => false,
            'wpautop'       => true,
            'label'      => __( 'Body Text', 'fl-builder' ),
            'default'    => ''
          )
        )
      ),

      'section--other-text' => array(
        'title'          => __( 'Other Text', 'fl-builder' ),
        'fields'         => array(
          'title'     => array(
            'type'       => 'text',
            'label'      => __( 'Title', 'fl-builder' ),
          ),
          'subtitle'     => array(
            'type'       => 'text',
            'label'      => __( 'Subtitle', 'fl-builder' ),
          ),
          'link_text'    => array(
            'type'       => 'text',
            'label'      => __( 'Link Text (under body text)', 'fl-builder' ),
          ),
          'link_url'     => array(
            'type'       => 'link',
            'label'      => __( 'URL', 'fl-builder' ),
          )
        )
      )
    )
  )
));
