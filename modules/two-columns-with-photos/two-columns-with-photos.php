<?php

/**
 * Custom Beaver Builder module: Two Columns with Photos.
 *
 * @class TwoColumnsWithPhotosModule
 */
class TwoColumnsWithPhotosModule extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Two Columns With Photos', 'fl-builder'),
            'description'   => __('Layout to be used for two columns with photos.', 'fl-builder'),
            'category'		=> __('Custom Modules', 'fl-builder'),
            'dir'           => CUSTOM_BB_MODULES_DIR . 'modules/two-columns-with-photos/',
            'url'           => CUSTOM_BB_MODULES_URL . 'modules/two-columns-with-photos/',
            'icon'          => 'format-image.svg',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('TwoColumnsWithPhotosModule', array(

  'heading-tab'          => array(
    'title'              => __( 'Heading', 'fl-builder' ),

    'sections'           => array(

      'section--title' => array(
        'title'          => __( 'Title & Subtitle', 'fl-builder' ),
        'fields'         => array(
          'title'        => array(
            'type'       => 'text',
            'label'      => __( 'Title', 'fl-builder' ),
          ),
          'subtitle'     => array(
            'type'       => 'text',
            'label'      => __( 'Subtitle', 'fl-builder' ),
          )
        )
      )
    )
  ),

  'column-one-tab'          => array(
    'title'              => __( 'Column One', 'fl-builder' ),

    'sections'           => array(

      'section__col_1' => array(
        'title'          => __( 'Column One', 'fl-builder' ),
        'fields' => array(
          'image__col_1'    => array(
            'type'          => 'photo',
            'label'         => __('Image', 'fl-builder'),
            'show_remove'   => false,
          ),
          'link_text__col_1' => array(
            'type'           => 'text',
            'label'          => __( 'Link Text (under body text)', 'fl-builder' ),
          ),
          'link_url__col_1' => array(
            'type'          => 'link',
            'label'         => __( 'URL', 'fl-builder' ),
          ),
          'body__col_1'     => array(
            'type'          => 'editor',
            'media_buttons' => false,
            'wpautop'       => true,
            'label'         => __( 'Body Text', 'fl-builder' ),
            'default'       => ''
          )
        )
      )
    )
  ),

  'column-two-tab'          => array(
    'title'              => __( 'Column Two', 'fl-builder' ),

    'sections'           => array(

      'section__col_2' => array(
        'title'          => __( 'Column Two', 'fl-builder' ),
        'fields'         => array(
          'image__col_2' => array(
            'type'          => 'photo',
            'label'         => __('Image', 'fl-builder'),
            'show_remove'   => false,
          ),
          'link_text__col_2'    => array(
            'type'       => 'text',
            'label'      => __( 'Link Text (under body text)', 'fl-builder' ),
          ),
          'link_url__col_2'     => array(
            'type'       => 'link',
            'label'      => __( 'URL', 'fl-builder' ),
          ),
          'body__col_2' => array(
            'type'          => 'editor',
            'media_buttons' => false,
            'wpautop'       => true,
            'label'      => __( 'Body Text', 'fl-builder' ),
            'default'    => ''
          )
        )
      )
    )
  )

));
