<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file:
 *
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example:
 */

?>

<?php

$video_only = 1;
$has_text = 0;

if ($settings->title || $settings->subtitle || $settings->body) :
  $video_only = 0;
  $has_text = 1;
endif;

$layout_classes = "";

if ($video_only) :
  $layout_classes .= " video-only";
endif;

if ($has_text) :
  $layout_classes .= " has-text";
endif;

//if ($settings->image_text_config) :
//  $layout = $settings->image_text_config;
//elseif ($video_only) :
//  $layout = 'image_only';
//else :
//  $layout = 'image_left';
//endif;
//
//$layout_class = str_replace("_","-",$layout);
//
//if ($layout == 'image_left' || $layout == 'image_right') :
//  $image_size = 'walsh-featured-project';
//else :
//  $image_size = 'walsh-work-slide';
//endif;
//
//if ($layout == 'image_only' || $layout == 'image_behind') :
//  $layout_class .= ' full-bleed';
//endif;
//
//echo $layout_class;

?>

<div class="bb-module bb-module--video-banner full-bleed<?php echo $layout_classes; ?>">

  <?php if ( $settings->video ) : ?>
    <div class="video-banner--video-wrapper">
      <video class="video-banner--video-container" autoplay loop muted plays-inline>
        <source src="<?php echo wp_get_attachment_url( $settings->video ); ?>" class="video-banner--video-src" type="video/mp4">
      </video>
      <div class="bg-overlay"></div>
      <div class="bg-underlay"></div>
    </div>
  <?php endif; ?>

  <?php if (!$video_only) : ?>
    <div class="video-banner--text-wrapper">
      <div class="module-header module-header--video-banner">
        <?php if ($settings->title) : ?>
          <h1 class="title title--video-banner"><?php echo $settings->title; ?></h1>
        <?php endif; ?>

        <?php if ($settings->subtitle) : ?>
          <h2 class="subtitle subtitle--video-banner"><?php echo $settings->subtitle; ?></h2>
        <?php endif; ?>
      </div>
      <?php if ($settings->body) : ?>
        <div class="body body--video-banner">
          <?php echo $settings->body; ?>
        </div>
      <?php endif; ?>
      <?php if ($settings->link_url && $settings->link_text) : ?>
        <div class="cta cta--video-banner">
          <a class="cta-link cta-link--video-banner" href="<?php echo $settings->link_url; ?>"><?php echo $settings->link_text; ?></a>
        </div>
      <?php endif; ?>
    </div>
  <?php endif; ?>

</div>
