<?php

/**
 * Custom Beaver Builder module: Featured Projects.
 *
 * @class FeaturedProjectsModule
 */
class FeaturedProjectsModule extends FLBuilderModule {

    /** 
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */  
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Featured Projects', 'fl-builder'),
            'description'   => __('Layout to be used for Featured Projects.', 'fl-builder'),
            'category'		=> __('Custom Modules', 'fl-builder'),
            'dir'           => CUSTOM_BB_MODULES_DIR . 'modules/featured-projects/',
            'url'           => CUSTOM_BB_MODULES_URL . 'modules/featured-projects/',
            'icon'          => 'format-image.svg',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
        ));
    }
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('FeaturedProjectsModule', array(

  'title-tab'          => array(
    'title'              => __( 'Title', 'fl-builder' ),
    'sections'           => array(
      'section--title' => array(
        'title'          => __( 'Block Title', 'fl-builder' ),
        'fields'         => array(
          'block_title'        => array(
            'type'       => 'text',
            'label'      => __( 'Title', 'fl-builder' ),
          )
        )
      )
    )
  ),

  'project-first-tab'          => array(
    'title'              => __( 'First Project', 'fl-builder' ),
    'sections'           => array(
      'section--title' => array(
        'title'          => __( 'Heading, text, image, and link', 'fl-builder' ),
        'fields'         => array(
          'title__project_first'        => array(
            'type'       => 'text',
            'label'      => __( 'Title', 'fl-builder' ),
          ),
          'subtitle__project_first'     => array(
            'type'       => 'text',
            'label'      => __( 'Subtitle', 'fl-builder' ),
          ),
          'image__project_first' => array(
            'type'          => 'photo',
            'label'         => __('Photo Field', 'fl-builder'),
            'show_remove'   => false,
          ),
          'link_text__project_first'    => array(
            'type'       => 'text',
            'label'      => __( 'Link Text', 'fl-builder' ),
          ),
          'link_url__project_first'     => array(
            'type'       => 'link',
            'label'      => __( 'URL', 'fl-builder' ),
          ),
          'body__project_first' => array(
            'type'          => 'editor',
            'media_buttons' => false,
            'wpautop'       => true,
            'label'      => __( 'Body Text', 'fl-builder' ),
            'default'    => ''
          )
        )
      )
    )
  ),

  'project-second-tab'          => array(
    'title'              => __( 'Second Project', 'fl-builder' ),
    'sections'           => array(
      'section--title' => array(
        'title'          => __( 'Heading, text, image, and link', 'fl-builder' ),
        'fields'         => array(
          'title__project_second'        => array(
            'type'       => 'text',
            'label'      => __( 'Title', 'fl-builder' ),
          ),
          'subtitle__project_second'     => array(
            'type'       => 'text',
            'label'      => __( 'Subtitle', 'fl-builder' ),
          ),
          'image__project_second' => array(
            'type'          => 'photo',
            'label'         => __('Photo Field', 'fl-builder'),
            'show_remove'   => false,
          ),
          'link_text__project_second'    => array(
            'type'       => 'text',
            'label'      => __( 'Link Text', 'fl-builder' ),
          ),
          'link_url__project_second'     => array(
            'type'       => 'link',
            'label'      => __( 'URL', 'fl-builder' ),
          ),
          'body__project_second' => array(
            'type'          => 'editor',
            'media_buttons' => false,
            'wpautop'       => true,
            'label'      => __( 'Body Text', 'fl-builder' ),
            'default'    => ''
          )
        )
      )
    )
  ),

  'project-third-tab'          => array(
    'title'              => __( 'Third Project', 'fl-builder' ),
    'sections'           => array(
      'section--title' => array(
        'title'          => __( 'Heading, text, image, and link', 'fl-builder' ),
        'fields'         => array(
          'title__project_third'        => array(
            'type'       => 'text',
            'label'      => __( 'Title', 'fl-builder' ),
          ),
          'subtitle__project_third'     => array(
            'type'       => 'text',
            'label'      => __( 'Subtitle', 'fl-builder' ),
          ),
          'image__project_third' => array(
            'type'          => 'photo',
            'label'         => __('Photo Field', 'fl-builder'),
            'show_remove'   => false,
          ),
          'link_text__project_third'    => array(
            'type'       => 'text',
            'label'      => __( 'Link Text', 'fl-builder' ),
          ),
          'link_url__project_third'     => array(
            'type'       => 'link',
            'label'      => __( 'URL', 'fl-builder' ),
          ),
          'body__project_third' => array(
            'type'          => 'editor',
            'media_buttons' => false,
            'wpautop'       => true,
            'label'      => __( 'Body Text', 'fl-builder' ),
            'default'    => ''
          )
        )
      )
    )
  )
));
