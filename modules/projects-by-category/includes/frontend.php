<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file:
 *
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * Example:
 */

?>

<div class="bb-module bb-module--projects-by-category">

  <div class="cols cols--projects-by-category">
    <div class="col col--first">
      <div class="image">
        <img src="<?php echo wp_get_attachment_image_src($settings->image, $size = "walsh-hover-image")[0]; ?>">
      </div>
    </div>
    <div class="col col--second">
      <div class="icon">
        <img src="<?php echo wp_get_attachment_image_src($settings->icon)[0]; ?>">
      </div>
      <div class="text">
        <h1 class="title"><?php echo $settings->title; ?></h1>
        <h2 class="subtitle"><?php echo $settings->subtitle; ?></h2>
        <div class="bodytext"><?php echo $settings->body; ?></div>
      </div>
    </div>
    <div class="col col--third">
      <h2 class="subtitle">Projects:</h2>
      <ul class="projects">
        <?php for ( $i = 0; $i < count( $settings->items ); $i++ ) : ?>
          <?php
          if ( empty( $settings->items[ $i ] ) ) {
            continue;
          }
          $label_id   = 'fl-accordion-' . $module->node . '-label-' . $i;
          $link_id = 'fl-accordion-' . $module->node . '-panel-' . $i;
          ?>
          <li class="project">
            <a href="<?php echo $settings->items[ $i ]->link; ?>"><?php echo $settings->items[ $i ]->label; ?></a>
          </li>
        <?php endfor; ?>
      </li>
    </div>
  </div>
</div>
